import fs from "fs";
import net from "net";
import log from "@ajar/marker";

const server = net.createServer();

server.on("connection", (socket) => {
  log.cyan("✨⚡ backup server connected ⚡✨");

  socket.on("data", async (buffer) => {
    const { data, fileName } = JSON.parse(buffer.toString().trim());
    // log.obj( { data, fileName } , "-> incoming:");

    const writeStream = fs.createWriteStream(`./${fileName}`, { encoding: "utf-8", flags: "a" });
    writeStream.write(data);
  });

  socket.on("end", () => {
    log.blue("⚡ backup server disconnected ⚡");
  });
});

server.on("error", (err) => log.error(err));

server.listen(8124, () => log.magenta("✨🚀 storage server is up 🚀✨"));
